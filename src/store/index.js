import { createStore } from 'vuex'

import authModule from './modules/auth/index.js'
import coachesModule from './modules/coaches/index.js'
import requestsModule from './modules/requests/index.js'

const store = createStore({
	modules: {
		auth: authModule, //using 'auth' namespace
		coaches: coachesModule, //using 'coaches' namespace
		requests: requestsModule //using 'requests' namespace
	}
})

export default store