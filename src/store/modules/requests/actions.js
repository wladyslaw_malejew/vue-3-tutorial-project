export default {
	async addRequest(context, payload) {
		const newRequest = {
			userEmail: payload.email,
			message: payload.message
		}

		const response = await fetch(
			`https://vue-3-tutorial-project-default-rtdb.europe-west1.firebasedatabase.app/requests/${payload.coachId}.json`, 
			{
				method: 'POST',
				body: JSON.stringify(newRequest)
			}
		)

		const responseData = await response.json()

		if(!response.ok) {
			const error = new Error(responseData.message || 'Failed to send request!')
			throw error
		}

		newRequest.id = responseData.name //setting a firebase generated id in local data
		newRequest.coachId = payload.coachId //setting a firebase generated coach id in local data

		context.commit('ADD_REQUEST', newRequest)
	},
	async fetchRequests(context) {
		const coachId = context.rootGetters.userId

		const token = context.rootGetters.token
		
		const response = await fetch(
			`https://vue-3-tutorial-project-default-rtdb.europe-west1.firebasedatabase.app/requests/${coachId}.json?auth=${token}`
		)
		const responseData = await response.json()

		if(!response.ok) {
			const error = new Error(responseData.message || 'Failed to fetch requests!')
			throw error
		}

		const requests = []

		for(const key in responseData) {
			const request = {
				id: key,
				coachId,
				userEmail: responseData[key].userEmail,
				message: responseData[key].message
			}

			requests.push(request)
		}

		context.commit('SET_REQUESTS', requests)
	}
}