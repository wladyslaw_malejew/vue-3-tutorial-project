export default {
	async registerCoach(context, payload) {
		const userId = context.rootGetters.userId

		const coachData = {
			firstName: payload.first,
			lastName: payload.last,
			description: payload.desc,
			hourlyRate: payload.rate,
			areas: payload.areas
		}

		const token = context.rootGetters.token

		const response = await fetch(
			`https://vue-3-tutorial-project-default-rtdb.europe-west1.firebasedatabase.app/coaches/${userId}.json?auth=${token}`, 
			{
				method: 'PUT',
				body: JSON.stringify(coachData)
			}
		)

		// const responseData = await response.json()

		if(!response.ok) {
			// error ...
		}

		context.commit('REGISTER_COACH', {
			...coachData,
			id: userId
		})
	},
	async loadCoaches(context, payload) {
		if(!payload.forceRefresh && !context.getters.shouldUpdate) { //if refresh button pressed and more than a minute passed
			return
		}

		const response = await fetch(
			`https://vue-3-tutorial-project-default-rtdb.europe-west1.firebasedatabase.app/coaches.json`
		)

		const responseData = await response.json()

		if(!response.ok) {
			const error = new Error(responseData.message || 'Failed to fetch!')
			throw error //now can be catched in a component where is dispatched
		}

		const coaches = []

		for(const key in responseData) { //correctly formatting coaches
			const coach = {
				id: key,
				firstName: responseData[key].firstName,
				lastName: responseData[key].lastName,
				description: responseData[key].description,
				hourlyRate: responseData[key].hourlyRate,
				areas: responseData[key].areas
			}

			coaches.push(coach)
		}

		context.commit('SET_COACHES', coaches)
		context.commit('SET_FETCH_TIMESTAMP')
	}
}